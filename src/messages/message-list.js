import React, { Component } from 'react';

import MessageView from './message-view';

class MessageList extends Component {
	
	state = {
		messages: [
			{
				from: 'Jidai',
				content: 'Have a great day!',
				status: 'read',
			},
			{
				from: 'Kaizoku',
				content: 'Hi There!',
				status: 'unread',
			},
		]
	}

	render() {
		const messageViews = this.state.messages.map(function(message, index){
			return (
				<MessageView key={index} message={message} />
			)
		});
		return (
			<div>
				<h1>List of Messages</h1>
				{messageViews}
			</div>
		)
	}
}


export default MessageList;