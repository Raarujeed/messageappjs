import React from 'react';
import ReactDom from 'react-dom';

import App from './App';

// const user = {
// 	firstName: "Jane",
// 	lastName: "Adam",
// };
// const element = <div>
// 	Hello Mr. <span>{user.lastName}</span>
// 	<p>Fullname: {user.firstName} {user.lastName}</p>
// 	</div> ;

ReactDom.render(<App />, document.getElementById('root'));